

from flask import Flask


application = Flask(__name__)


@application.route('/', methods=['GET'])
def home():
    return "<h1>Hello World Flask ...</h1>"


if __name__ == '__main__':
    application.run()
    # application.run(debug=True, host="0.0.0.0")
