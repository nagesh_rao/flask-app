FROM ubuntu
RUN apt-get update -y && \
    apt-get install -y python3-pip python-dev

COPY ./req.txt /app/req.txt

WORKDIR /app

RUN pip3 install -r req.txt

RUN pip3 install flask

COPY . /app

ENTRYPOINT [ "python3" ]

CMD ["application.py"]